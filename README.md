# Inspeccionando el Core de Odoo

[![N|UNEFA](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Estudiantes de 6to Semestre de la Universidad Nacional Experimental de La Fuerza Armada.

  - Yleana Margarita, exceptions.py 
  - Alfonso Monroy, sql_db.py
  - karen Polo, api.py
  - Ibrahim Marrero, http.py
  - Wladimir Suarez, fields.py
  - Carlos Torrealba, models.py
  - Kevin Martínez, netsvc.py


### Objetivo General

Conocer Odoo desde su motor.

### Objetivos Especificos.

* Cada estudiante debe crear una rama con el nombre del archivo asignado y agregar el archivo, luego debe hacer una inspección a cada linea del código y describir dentro del mismo el objetivo de dichas lineas o bloques de líneas.
* Cada miércoles en clase debe dar parte de los avances a sus compañeros.
* Comprender el objetivo y la importancia del contenido del archivo para el framework.
* Aprender tecnicas de desarrollo.
* Hacer aportes y dejarlos en la descrición.
